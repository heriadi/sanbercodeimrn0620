// No. 1 Looping While
console.log('======LOOPING PERTAMA======')

var loop1 = 2;
while(loop1 < 22) { 
  console.log(loop1+ ' - I love coding'); 
  loop1+= 2; 
}

console.log('\n ======LOOPING KEDUA======')

var loop2 = 22;
while(loop2 > 0) { 
  loop2 -= 2;
  console.log(loop2+ ' - I will become a mobile developer')
}

console.log('\n =====LOOPING FOR=====')

var a  = 1;

for (i = a; i<= 20; i++) {
  if( (i%3) === 0 && (i%2) === 1) {
    console.log(i+ ' - I Love Coding');
  } else if ( (i%2) === 0) {
    console.log(i+ ' - Bekualitas')
  } else {
    console.log(i+ ' - Santai')
  }
}


console.log('\n=====Membuat Persegi Panjang #=====')

var h = '';

for (var i = 0; i < 4; i++) {
  for( var j = 0; j < 8; j++ ) {
    h += '#';
  }
  h += '\n';
}
console.log(h)

console.log('=====Membuat Tangga=====')

var c = '';

for (var a = 0; a < 7; a++) {
  for( var b = 0; b <= a; b++ ) {
    c += '#';
  }
  c += '\n';
}
console.log(c)

console.log('=====Membuat Papan Catur=====')
var d = '';

for (var e = 0; e < 8; e++) {
  for( var f = 0; f < 4; f++ ) {
    if ((f % 2) !== 0) {
      d += ' ';
    } else if ((e % 2) !== 0) {
      d += ' ';
    }
    d += '#'+' ';
  }
  d += '\n';

}
console.log(d)