console.log();
console.log('------1. Mengubah fungsi menjadi fungsi arrow------');
console.log();

// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }
   
//   golden()

golden = () => {
    return 'this is golden!!'
};
console.log(golden());

console.log();
console.log('------2. Sederhanakan menjadi Object literal di ES6------');
console.log();

// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }
   
//   //Driver Code 
//   newFunction("William", "Imoh").fullName() 

const firstName = 'William';
const lastName = 'Imoh';

const fullName = `${firstName} ${lastName}`

console.log(fullName);

console.log();
console.log('------3. Destructuring------');
console.log();

// const newObject = {
//     firstName: "Harry",
//     lastName: "Potter Holt",
//     destination: "Hogwarts React Conf",
//     occupation: "Deve-wizard Avocado",
//     spell: "Vimulus Renderus!!!"
//   }
//   const firstName = newObject.firstName;
//   const lastName = newObject.lastName;
//   const destination = newObject.destination;
//   const occupation = newObject.occupation;

let newObject = {
    firstName2: "Harry",
    lastName2: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };
  const {firstName2, lastName2, destination, occupation, spell} = newObject

  console.log(firstName2, lastName2, destination, occupation, spell);

console.log();
console.log('------4. Array Spreading------');
console.log();

// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

console.log(combined);

console.log();
console.log('------5. Template Literals------');
console.log();

// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
 
// // Driver Code
// console.log(before)

const planet = "earth"
const view = "glass"

const before = `Lorem ${planet} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before)