console.log()
console.log('----------1. Animal Class----------')
console.log()

console.log()
console.log('====Release 0====')
console.log()

class Animal {
    constructor(name) {
        this.animalName = name;
        this._cold_blooded = false;
        this._legs = 4;
    }
    get name (){
        return this.animalName;
    }
    get legs () {
        return this._legs;
    }
    get cold_blooded () {
        return this._cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log()
console.log('=====Release 1====')
console.log()

class Ape extends Animal {
    constructor (name) {
        super(name);
        this.character = '"Auooo"';
    }
    yell() {
        return this.character;
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
        this.hop ='"hop hop"';
    }
    jump () {
        return this.hop;
    }
}

var sungokong = new Ape("kera sakti");
console.log(sungokong.yell()); // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 

console.log()
console.log('----------2. Function to Class----------')
console.log()

class Clock {
  constructor({template}) {
    this.template = template;
    this.timer;
  }
  
     render() {
      this.date = new Date();
  
      this.hours = this.date.getHours();
      if (this.hours < 10) this.hours = '0' + this.hours;
  
      this.mins = this.date.getMinutes();
      if (this.mins < 10) this.mins = '0' + this.mins;
  
      this.secs = this.date.getSeconds();
      if (this.secs < 10) this.secs = '0' + this.secs;
  
      this.output = this.template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);
  
      console.log(this.output);
    }
  
    stop () {
      clearInterval(this.timer);
    };
  
    start () {
        this.render()
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 