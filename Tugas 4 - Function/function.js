console.log('\n');
console.log('====Soal Nomor 1====');

function teriak() {
    return ('Halo Sanbers!')
}
var sanbers = teriak();
console.log(sanbers);

console.log('\n');
console.log('====Soal Nomor 2====');

var num1 = 12;
var num2 = 4;

function multiply() {
    return (num1*num2)
}
var hasil = multiply();
console.log(hasil);

console.log('\n');
console.log('====Soal Nomor 3====');

var name = 'Heriadi';
var age = 30;
var address = 'Jln. Malioboro, Yogyakarta';
var hobby = 'Gaming';

function  introduce() {
    var a = ('Nama saya ' +name+ ', umur saya ' +age+ ' tahun alamat saya di ' +address+ ', dan saya punya hobby yaitu ' +hobby);
    return (a)
}
var kalimat = introduce();
console.log(kalimat);