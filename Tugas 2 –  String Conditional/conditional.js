// Soal If Else
function soalIfElse(nama, peran) {
    if (nama == "") {
        console.log("Nama harus diisi!")
    } else if (nama && peran == "") {
        console.log("Halo" +nama+ ", Pilih peranmu untuk memulai game!")
    } else if (nama == "Heriadi" && peran == "Penyihir") {
        console.log("Selamat datang di Dunia Werewolf, Heriadi \nHalo Penyihir Heriadi, kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (nama == "Erick" && peran == "Guard") {
        console.log("Selamat datang di Dunia Werewolf, Erick \nHalo Guard Erick, kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (nama == "Erhy" && peran == "WereWolf") {
        console.log("Selamat datang di Dunia Werewolf, Erhy \nHalo Werewolf Erhy, Kamu akan memakan mangsa setiap malam!")
    }
}

soalIfElse("", "")
soalIfElse("Heriadi", "")
soalIfElse("Heriadi", "Penyihir")
soalIfElse("Erick", "Guard")
soalIfElse("Erhy", "WereWolf")

// Soal Switch Case
var tanggal = 1;
var bulan = 5;
var tahun = 1945;
var teksBulan;

switch (true) {
    case (tanggal < 1 || tanggal > 31): {
        console.log("Tanggal salah")
        break;
    }
    case (bulan > 12 || tahun < 1): {
        console.log("Bulan salah")
        break;
    }
    case (tahun < 1900 || tahun > 2200):
        console.log("Tahun salah")
        break;
        default:
            {
                switch (true) {
                    case bulan == 1:
                        teksBulan = "Januari";
                        break;
                    case bulan == 2:
                        teksBulan = "Februari";
                        break;
                    case bulan == 3:
                        teksBulan = "Maret";
                        break;
                    case bulan == 4:
                        teksBulan = "April";
                        break;
                    case bulan == 5:
                        teksBulan = "Mei";
                        break;
                    case bulan == 6:
                        teksBulan = "Juni";
                        break;
                    case bulan == 7:
                        teksBulan = "Juli";
                        break;
                    case bulan == 8:
                        teksBulan = "Agustus";
                        break;
                    case bulan == 9:
                        teksBulan = "September";
                        break;
                    case bulan == 10:
                        teksBulan = "Oktober";
                        break;
                    case bulan == 11:
                        teksBulan = "November";
                        break;
                    case bulan == 12:
                        teksBulan = "Desember";
                        break;
                    default:
                        break;
                }
                console.log(tanggal, " ", teksBulan, " ", tahun)
                break;
            }
}