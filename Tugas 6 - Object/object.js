console.log('-------No. 1 (Array to Object)--------')

console.log()
console.log()

function arrayToObject(firstName, lastName, gender, age) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    if (thisYear > age) {
        thisYear - age 
    } else {
        age = "Invalid Birth year"
    }

    var arr = {};
    arr.firstName = firstName;
    arr.lastName = lastName;
    arr.gender = gender;
    arr.age = age;
    return arr;
}
 
// Driver Code
var people = arrayToObject("Bruce", "Banner", "male", 1975)
var people2 = arrayToObject("Natasha", "Romanoff", "female")
console.log(people)
console.log(people2)

/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
// arrayToObject(people2) 
// /*
//     1. Tony Stark: { 
//         firstName: "Tony",
//         lastName: "Stark",
//         gender: "male",
//         age: 40
//     }
//     2. Pepper Pots: { 
//         firstName: "Pepper",
//         lastName: "Pots",
//         gender: "female".
//         age: "Invalid Birth Year"
//     }
// */
 
// // Error case 
// arrayToObject([]) // ""